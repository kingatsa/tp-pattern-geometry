package org.acme.geometry;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class EnvelopeBuilder {
    private List<Double> xVals;
    private List<Double> yVals;


    public EnvelopeBuilder(){
        this.xVals = new ArrayList<Double>();
        this.yVals = new ArrayList<Double>();
    }

    public void insert(Coordinate coordinate){
        this.xVals.add(coordinate.getX());
        this.yVals.add(coordinate.getY());
    }

    public Envelope build(){
        Coordinate bottomLeft = new Coordinate(Collections.min(this.xVals), Collections.min(this.yVals));
        Coordinate topRight = new Coordinate(Collections.max(this.xVals), Collections.max(this.yVals));
        return new Envelope(bottomLeft, topRight);
    }
}

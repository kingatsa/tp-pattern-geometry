package org.acme.geometry;

import java.util.ArrayList;
import java.util.List;

public class LineString implements Geometry{
    
    private List<Point> points;

    public LineString(){
        this.points = new ArrayList<Point>();
    }

    public LineString(List<Point> points){
        this.points = points;
    }

    public int getNumPoints(){
        return this.points.size();
    }

    public Point getPointN(int n){
        return this.points.get(n);
    }

    @Override
    public String getType(){
        return "LINESTRING";
    }

    @Override
    public boolean isEmpty(){
        return this.points.isEmpty();
    }

    @Override
    public void translate(double dx, double dy){
        for (int i=0; i<this.points.size(); i++){
            this.points.get(i).translate(dx, dy);
        }
    }

    @Override
    public LineString clone(){
        List<Point> clonedLineString = new ArrayList<Point>();
        for (int i=0; i<this.points.size(); i++){
            clonedLineString.add((Point) this.points.get(i).clone());
        }
        return new LineString(clonedLineString);
    }

    @Override
    public Envelope getEnvelope(){
        EnvelopeBuilder eB = new EnvelopeBuilder();
        for (int i = 0; i<this.points.size(); i++){
            eB.insert(this.points.get(i).getCoordinate());
        }
        return eB.build();
    }

    @Override
    public void accept(GeometryVisitor visitor) {
        visitor.visit(this);
        
    }
    
}

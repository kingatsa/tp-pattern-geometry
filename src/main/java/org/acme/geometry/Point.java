package org.acme.geometry;


public class Point implements Geometry {
    private Coordinate coordinate;

    public Point(){
        this.coordinate = new Coordinate();
    }

    public Point(Coordinate coordinate){
        this.coordinate = coordinate;
    }

    public Coordinate getCoordinate(){
        return this.coordinate;
    }

    @Override
    public String getType(){
        return "POINT";
    }

    @Override
    public boolean isEmpty(){
        return this.coordinate.isEmpty();
    }

    @Override
    public void translate(double dx, double dy){
        double xVal = this.coordinate.getX();
        double yVal = this.coordinate.getY();
        this.coordinate = new Coordinate(xVal+dx, yVal+dy);
    }

    @Override
    public Point clone(){
        return new Point(this.coordinate);
    }

    @Override
    public Envelope getEnvelope(){
        return new Envelope(this.coordinate, this.coordinate);
    }

    @Override
    public void accept(GeometryVisitor visitor) {
        visitor.visit(this);
        
    }
}

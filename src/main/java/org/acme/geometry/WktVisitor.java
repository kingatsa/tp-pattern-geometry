package org.acme.geometry;

public class WktVisitor implements GeometryVisitor {

    private StringBuffer buffer; 

    public WktVisitor(){
        this.buffer = new StringBuffer();
    }

    public String getResult(){
        return this.buffer.toString();
    }

    @Override
    public void visit(Point point) {
        if(point.isEmpty()){
            this.buffer.append(point.getType() + " EMPTY");
        } else {
            this.buffer.append(point.getType() + "(" + point.getCoordinate().getX() + " " + point.getCoordinate().getY() + ")");
        }
        
    }

    @Override
    public void visit(LineString lineString) {
        if(lineString.isEmpty()){
            this.buffer.append(lineString.getType() + " EMPTY");
        } else {
            this.buffer.append(lineString.getType() + "(");
            String s = "";
            for (int i = 0; i<lineString.getNumPoints(); i++){
                s += lineString.getPointN(i).getCoordinate().getX() + " " + lineString.getPointN(i).getCoordinate().getY() + ",";
            }
            s = s.substring(0, s.length() - 1);
            s +=  ")";
            buffer.append(s);
        }
        
    }

}

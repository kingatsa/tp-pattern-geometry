package org.acme.geometry;

public class WktWriter {
    
    public WktWriter(){

    }

    public String write(Geometry geometry){
        String s = "";
        if ( geometry instanceof Point ){
            Point point = (Point)geometry;
            if(point.isEmpty()){
                s += point.getType() + " EMPTY";
            } else {
                s += point.getType() + "(" + point.getCoordinate().getX() + " " + point.getCoordinate().getY() + ")";
            }
        }else if ( geometry instanceof LineString ){
            LineString lineString = (LineString)geometry;
            if(lineString.isEmpty()){
                s += lineString.getType() + " EMPTY";
            } else {
                s += lineString.getType() + "(";
                for (int i = 0; i<lineString.getNumPoints(); i++){
                    s += lineString.getPointN(i).getCoordinate().getX() + " " + lineString.getPointN(i).getCoordinate().getY() + ",";
                }
                s = s.substring(0, s.length() - 1);
                s +=  ")";
            }
             
        }else{
            throw new RuntimeException("geometry type not supported");
        }
        return s;
    }
}

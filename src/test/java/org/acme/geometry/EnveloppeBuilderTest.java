package org.acme.geometry;

import org.junit.Assert;
import org.junit.Test;

public class EnveloppeBuilderTest {

    public static final double EPSILON = 1.0e-15;

    @Test
	public void testInsertBuild(){
		EnvelopeBuilder eB = new EnvelopeBuilder();
        eB.insert(new Coordinate(0.0,1.0));
        eB.insert(new Coordinate(10.0,2.0));
        eB.insert(new Coordinate(-5.0,6.0));
        Envelope e = eB.build();
        Assert.assertFalse(e.isEmpty());
        Assert.assertEquals(-5.0, e.getXmin(), EPSILON); 
        Assert.assertEquals(10.0, e.getXmax(), EPSILON); 
        Assert.assertEquals(1.0, e.getYmin(), EPSILON); 
        Assert.assertEquals(6.0, e.getYmax(), EPSILON); 

	}
}

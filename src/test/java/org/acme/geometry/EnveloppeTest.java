package org.acme.geometry;

import org.junit.Assert;
import org.junit.Test;


public class EnveloppeTest {
    public static final double EPSILON = 1.0e-15;

	@Test
	public void testDefaultConstructor(){
		Envelope e = new Envelope();
        Assert.assertTrue(e.isEmpty());
	
	}

	@Test
	public void testConstructor(){
        Coordinate bottomLeft = new Coordinate(1.0,6.0);
        Coordinate topRight = new Coordinate(7.0, 10.0);
		Envelope e = new Envelope(bottomLeft, topRight);
        Assert.assertFalse(e.isEmpty());
        Assert.assertEquals(1.0, e.getXmin(), EPSILON); 
        Assert.assertEquals(7.0, e.getXmax(), EPSILON); 
        Assert.assertEquals(6.0, e.getYmin(), EPSILON); 
        Assert.assertEquals(10.0, e.getYmax(), EPSILON); 
    }
}






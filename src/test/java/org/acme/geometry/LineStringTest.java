package org.acme.geometry;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class LineStringTest {

	public static final double EPSILON = 1.0e-15;

    @Test
	public void testDefaultConstructor(){
        LineString ls = new LineString();
        Assert.assertEquals(0, ls.getNumPoints(), EPSILON);
    }

    @Test
	public void testConstructor(){

        ArrayList<Point> pts = new ArrayList<Point>();
        Point a = new Point();
        Point b = new Point(new Coordinate(1.0, 2.0));
        pts.add(a);
        pts.add(b);
        LineString ls = new LineString(pts);

        Assert.assertEquals(2, ls.getNumPoints(), EPSILON);
        Assert.assertSame(b, ls.getPointN(1));
    }

    @Test
	public void testType(){        
        LineString ls = new LineString();
        Assert.assertEquals("LINESTRING", ls.getType());
    }

    @Test
	public void testDefaultIsEmpty(){
		LineString ls = new LineString();
		Assert.assertTrue(ls.isEmpty());
	}

    @Test
	public void testIsEmpty(){
        ArrayList<Point> pts = new ArrayList<Point>();
        Point a = new Point(new Coordinate(0.0, 0.0));
        Point b = new Point(new Coordinate(1.0, 2.0));
        pts.add(a);
        pts.add(b);
        LineString ls = new LineString(pts);
		Assert.assertFalse(ls.isEmpty());
	}

    @Test
    public void testTranslate(){
        ArrayList<Point> pts = new ArrayList<Point>();
        pts.add(new Point(new Coordinate(1.0, 2.0)));
        pts.add(new Point(new Coordinate(1.0, 2.0)));
        LineString ls = new LineString(pts);
        ls.translate(9.0, 100.0);
        for (int i = 0; i<ls.getNumPoints(); i++){
            Assert.assertEquals(10.0, ls.getPointN(i).getCoordinate().getX(), EPSILON);
            Assert.assertEquals(102.0, ls.getPointN(i).getCoordinate().getY(), EPSILON);
        }
    }

    @Test
    public void testClone(){
        ArrayList<Point> pts = new ArrayList<Point>();
        pts.add(new Point(new Coordinate(1.0, 2.0)));
        pts.add(new Point(new Coordinate(3.0, 4.0)));
        LineString ls = new LineString(pts);
        LineString lsClone = (LineString) ls.clone();
        Assert.assertEquals(ls.getNumPoints(), lsClone.getNumPoints(), EPSILON);

        for (int i = 0; i<ls.getNumPoints(); i++){
            Assert.assertEquals(ls.getPointN(i).getCoordinate().getX(), 
                                lsClone.getPointN(i).getCoordinate().getX(), 
                                EPSILON);
            Assert.assertEquals(ls.getPointN(i).getCoordinate().getY(), 
                                lsClone.getPointN(i).getCoordinate().getY(), 
                                EPSILON);
        }
    }

    @Test
    public void testGetEnveloppe(){
        ArrayList<Point> pts = new ArrayList<Point>();
        pts.add(new Point(new Coordinate(1.0, 2.0)));
        pts.add(new Point(new Coordinate(3.0, 6.0)));
        pts.add(new Point(new Coordinate(5.0, 4.0)));
        LineString ls = new LineString(pts);
        Envelope e = ls.getEnvelope();
        Assert.assertEquals(1.0, e.getXmin(), EPSILON); 
        Assert.assertEquals(5.0, e.getXmax(), EPSILON); 
        Assert.assertEquals(2.0, e.getYmin(), EPSILON); 
        Assert.assertEquals(6.0, e.getYmax(), EPSILON); 
    

    }

}

package org.acme.geometry;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

public class LogGeometryVisitorTest {

    public static final double EPSILON = 1.0e-15;

    @Test
    public void testVisitorPoint() throws UnsupportedEncodingException{
        
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(os);
        LogGeometryVisitor visitor = new LogGeometryVisitor(out);
        Geometry geometry = new Point(new Coordinate(2.0,3.0));
        geometry.accept(visitor);
        String result = os.toString("UTF8");
        System.out.println(result);
        Assert.assertEquals("Je suis un point avec x=2.0 et y=3.0", result);

    }

    @Test
    public void testVisitorLineString() throws UnsupportedEncodingException{
        
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(os);
        LogGeometryVisitor visitor = new LogGeometryVisitor(out);

        ArrayList<Point> pts = new ArrayList<Point>();
        pts.add(new Point(new Coordinate(1.0, 2.0)));
        pts.add(new Point(new Coordinate(3.0, 6.0)));
        pts.add(new Point(new Coordinate(5.0, 4.0)));
        
        Geometry geometry = new LineString(pts);
        geometry.accept(visitor);
        String result = os.toString("UTF8");
        System.out.println(result);
        Assert.assertEquals("Je suis une polyligne définie par 3 point(s)", result);

    }
}

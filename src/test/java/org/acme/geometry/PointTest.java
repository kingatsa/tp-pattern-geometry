package org.acme.geometry;

import org.junit.Assert;
import org.junit.Test;

public class PointTest {

	public static final double EPSILON = 1.0e-15;

    @Test
	public void testDefaultConstructor(){

        Point p = new Point();
        Assert.assertEquals(Double.NaN, p.getCoordinate().getX(), EPSILON);
        Assert.assertEquals(Double.NaN, p.getCoordinate().getY(), EPSILON);
    }

    @Test
	public void testConstructor(){
        Coordinate coord = new Coordinate(156.0, 255.5);
        Point p = new Point(coord);
        Assert.assertEquals(156.0, p.getCoordinate().getX(), EPSILON);
        Assert.assertEquals(255.5, p.getCoordinate().getY(), EPSILON);
    }

    @Test
	public void testConstructorCoordinate(){        
        Coordinate coord = new Coordinate(156.0, 255.5);
        Point p = new Point(coord);
        Assert.assertSame(coord, p.getCoordinate());
    }

    @Test
	public void testDefaultType(){
        Point p = new Point();
        Assert.assertEquals("POINT", p.getType());
    }

    @Test
	public void testType(){        
        Coordinate coord = new Coordinate(156.0, 255.5);
        Point p = new Point(coord);
        Assert.assertEquals("POINT", p.getType());
    }

    @Test
	public void testDefaultIsEmpty(){
		Point p = new Point();
		Assert.assertTrue(p.isEmpty());
	}

    @Test
	public void testIsEmpty(){
		Coordinate coord = new Coordinate(156.0, 255.5);
        Point p = new Point(coord);
		Assert.assertFalse(p.isEmpty());
	}
    
    @Test
    public void testTranslate(){
        Coordinate coord = new Coordinate(156.0, 255.5);
        Point p1 = new Point(coord);
        p1.translate(100.0, 0.5);
        Assert.assertEquals(256.0, p1.getCoordinate().getX(), EPSILON);
		Assert.assertEquals(256.0, p1.getCoordinate().getY(), EPSILON);
	
    }

    @Test
    public void testDefaultClone(){
        Point p = new Point();
        Point pClone = (Point) p.clone();
        Assert.assertEquals(p.getCoordinate().getX(), pClone.getCoordinate().getX(), EPSILON);
        Assert.assertEquals(p.getCoordinate().getY(), pClone.getCoordinate().getY(), EPSILON);
    }

    @Test
    public void testClone(){
        Coordinate coord = new Coordinate(156.0, 255.5);
        Point p = new Point(coord);
        Point pClone = (Point) p.clone();
        Assert.assertEquals(p.getCoordinate().getX(), pClone.getCoordinate().getX(), EPSILON);
        Assert.assertEquals(p.getCoordinate().getY(), pClone.getCoordinate().getY(), EPSILON);
    }

    @Test
    public void testGetEnvelope(){
        Coordinate coord = new Coordinate(156.0, 255.5);
        Point p = new Point(coord);
        Envelope e = p.getEnvelope();
        Assert.assertEquals(156.0, e.getXmin(), EPSILON); 
        Assert.assertEquals(156.0, e.getXmax(), EPSILON); 
        Assert.assertEquals(255.5, e.getYmin(), EPSILON); 
        Assert.assertEquals(255.5, e.getYmax(), EPSILON); 
    }
}

package org.acme.geometry;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

public class WktVisitorTest {
    public static final double EPSILON = 1.0e-15;

    @Test
    public void testWktDefaultVisitorPoint() throws UnsupportedEncodingException{
        
        WktVisitor visitor = new WktVisitor();
        Geometry geometry = new Point();
        geometry.accept(visitor);
        Assert.assertEquals( "POINT EMPTY", visitor.getResult() );

    }

    @Test
    public void testWktVisitorPoint() throws UnsupportedEncodingException{
        
        WktVisitor visitor = new WktVisitor();
        Geometry geometry = new Point(new Coordinate(3.0,4.0));
        geometry.accept(visitor);
        Assert.assertEquals( "POINT(3.0 4.0)", visitor.getResult() );

    }

    @Test
    public void testDefaultWktVisitorLineString() throws UnsupportedEncodingException{
        
        WktVisitor visitor = new WktVisitor();        
        Geometry geometry = new LineString();
        geometry.accept(visitor);
        String result = visitor.getResult();
        System.out.println(result);
        Assert.assertEquals("LINESTRING EMPTY", result.trim());
    }

    @Test
    public void testWktVisitorLineString() throws UnsupportedEncodingException{
        

        WktVisitor visitor = new WktVisitor();

        ArrayList<Point> pts = new ArrayList<Point>();
        pts.add(new Point(new Coordinate(0.0, 0.0)));
        pts.add(new Point(new Coordinate(1.0, 1.0)));
        pts.add(new Point(new Coordinate(5.0, 5.0)));
        
        Geometry geometry = new LineString(pts);
        geometry.accept(visitor);
        String result = visitor.getResult();
        System.out.println(result);
        Assert.assertEquals("LINESTRING(0.0 0.0,1.0 1.0,5.0 5.0)", result.trim());
    }
}

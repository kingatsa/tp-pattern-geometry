package org.acme.geometry;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;


public class WktWriterTest {
    
	public static final double EPSILON = 1.0e-15;

    @Test
    public void testWritePoint(){
        Geometry g = new Point(new Coordinate(3.0,4.0));
        WktWriter writer = new WktWriter();
        System.out.println(writer.write(g));
        Assert.assertEquals("POINT(3.0 4.0)", writer.write(g));
    }

    @Test
    public void testWritePointEmpty(){
        Geometry g = new Point();
        WktWriter writer = new WktWriter();
        System.out.println(writer.write(g));
        Assert.assertEquals("POINT EMPTY", writer.write(g));
    }

    @Test
    public void testWriteLineString(){
        ArrayList<Point> pts = new ArrayList<Point>();
        pts.add(new Point(new Coordinate(0.0, 0.0)));
        pts.add(new Point(new Coordinate(1.0, 1.0)));
        pts.add(new Point(new Coordinate(5.0, 5.0)));

        Geometry g = new LineString(pts);
        WktWriter writer = new WktWriter();
        System.out.println(writer.write(g));
        Assert.assertEquals("LINESTRING(0.0 0.0,1.0 1.0,5.0 5.0)", writer.write(g));
    }

    @Test
    public void testWriteLineStringEmpty(){
        Geometry g = new LineString();
        WktWriter writer = new WktWriter();
        System.out.println(writer.write(g));
        Assert.assertEquals("LINESTRING EMPTY", writer.write(g));
    }

   


}
